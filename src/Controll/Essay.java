package Controll;

import java.util.LinkedList;

public class Essay {

    LinkedList<Sentence> sentenceList = new LinkedList<Sentence>();
    String text = "";

    public Essay(){
        Sentence currentSentence = new Sentence();
        sentenceList.add(currentSentence);
    }

    public void addLetter(Letter letter) {
        if (letter.getText() != null) {
            if (letter.getText().equals(".") || letter.getText().equals("!") || letter.getText().equals("?")) {
                System.out.println("Neuer Satz");
                sentenceList.getLast().setSentenceEnding(letter.getText());
                Sentence currentSentence = new Sentence();
                sentenceList.add(currentSentence);
            }
            else {
                System.out.println("Kein Satzende");
                sentenceList.getLast().addLetter(letter);
            }
        }
    }

    public LinkedList<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
