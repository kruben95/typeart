package Controll;

public class Multiplikator {

    private double xMulti = 1.0;
    private double xMulti2 = 1.0;
    private double yMulti = 1.0;
    private double multiHeight = 1.0;
    private double multiWidth = 1.0;
    private double multiRed = 1.0;
    private double multiGreen = 1.0;
    private double multiBlue = 1.0;

    public Multiplikator(){}

    public Multiplikator(double xMulti, double yMulti, double multiHeight, double multiWidth) {
        this.xMulti = xMulti;
        this.yMulti = yMulti;
        this. multiHeight = multiHeight;
        this.multiWidth = multiWidth;
    }


    public double getxMulti() {
        return xMulti;
    }

    public void setxMulti(double xMulti) {
        this.xMulti = xMulti;
    }

    public double getxMulti2() {
        return xMulti2;
    }

    public void setxMulti2(double xMulti2) {
        this.xMulti2 = xMulti2;
    }

    public double getyMulti() {
        return yMulti;
    }

    public void setyMulti(double yMulti) {
        this.yMulti = yMulti;
    }

    public double getMultiHeight() {
        return multiHeight;
    }

    public void setMultiHeight(double multiHeight) {
        this.multiHeight = multiHeight;
    }

    public double getMultiWidth() {
        return multiWidth;
    }

    public void setMultiWidth(double multiWidth) {
        this.multiWidth = multiWidth;
    }

    public double getMultiRed() {
        return multiRed;
    }

    public void setMultiRed(double multiRed) {
        this.multiRed = multiRed;
    }

    public double getMultiGreen() {
        return multiGreen;
    }

    public void setMultiGreen(double multiGreen) {
        this.multiGreen = multiGreen;
    }

    public double getMultiBlue() {
        return multiBlue;
    }

    public void setMultiBlue(double multiBlue) {
        this.multiBlue = multiBlue;
    }
}

