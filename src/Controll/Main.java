package Controll;

import Geo.Geo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.control.ColorPicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;

public class Main extends Application {

    Scene scene;
    PaneManager paneManager;
    Essay essay = new Essay();
    CheckBox overlayCB;
    CheckBox drawLettersCB;
    CheckBox rotationCB;
    Stage primaryStage;
    MenuBar menuBar;

    Integer imageCounter;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
        primaryStage.setTitle("TypeArt");
        scene = new Scene(root, 1000, 800, true, SceneAntialiasing.BALANCED);
        primaryStage.setMinWidth(350);
        primaryStage.setMinHeight(350);
        this.primaryStage = primaryStage;
        //scene.setFill(Color.RED);
        scene.getStylesheets().add("/View/style.css");



        //Color Picker
        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.WHITE);



        paneManager = new PaneManager(this, scene, essay);


        //Create Sliders and configure
        Slider geoHeightSlider = new Slider(.5,2,1);
            geoHeightSlider.setShowTickMarks(true);
            geoHeightSlider.setShowTickLabels(true);
            geoHeightSlider.setMajorTickUnit(10);
            geoHeightSlider.setMinorTickCount(1);
            geoHeightSlider.setBlockIncrement(.1);
        Slider geoWidthSlider = new Slider(.5,2,1);
            geoWidthSlider.setShowTickMarks(true);
            geoWidthSlider.setShowTickLabels(true);
            geoWidthSlider.setMajorTickUnit(10);
            geoWidthSlider.setMinorTickCount(1);
            geoWidthSlider.setBlockIncrement(.1);

        Slider geoAbstandWidthSlider = new Slider(.5,6,1);
            geoAbstandWidthSlider.setShowTickMarks(true);
            geoAbstandWidthSlider.setShowTickLabels(true);
            geoAbstandWidthSlider.setMajorTickUnit(10);
            geoAbstandWidthSlider.setMinorTickCount(1);
            geoAbstandWidthSlider.setBlockIncrement(.1);

        Slider geoAbstandHeightSlider = new Slider(.5,6,1);
            geoAbstandHeightSlider.setShowTickMarks(true);
            geoAbstandHeightSlider.setShowTickLabels(true);
            geoAbstandHeightSlider.setMajorTickUnit(10);
            geoAbstandHeightSlider.setMinorTickCount(1);
            geoAbstandHeightSlider.setBlockIncrement(.1);



        //MenuBar
        menuBar = ((MenuBar) scene.lookup("#menuBar"));

        //Save Load
        Menu menu1 = new Menu("File");
            MenuItem menuSave = new MenuItem("Save");
            MenuItem menuLoad = new MenuItem("Load");
            menu1.getItems().add(menuSave);
            menu1.getItems().add(menuLoad);

        menuBar.getMenus().add(menu1);

        //Malen-Button
        Button mainSwitch = new Button("Malen");
        Menu drawButton = new Menu("", mainSwitch);

        //Checkboxen
        drawLettersCB = new CheckBox();
        overlayCB = new CheckBox();
        rotationCB = new CheckBox();
        rotationCB.setSelected(true);

        MenuItem rotaionMenu = new MenuItem("Mit Rotation ", rotationCB);
        Menu showOverlay = new Menu("Ovelay anzeigen ", overlayCB);
        Menu showText = new Menu("Text anzeigen ", drawLettersCB);
        //Color Picker Menu
        Menu colorPickers = new Menu(null, colorPicker);

        //Konfigurationen
        Menu configuration = new Menu("Konfigurationen");
            Image newImage = new Image("images/confi.png",25,25,false,false);
            configuration.setGraphic(new ImageView(newImage));
        Menu geoHeightMenu = new Menu("Objekthöhe");
        Menu geoWidthMenu = new Menu("Objektbreite");
        Menu geoAbstandWidthMenu = new Menu("Abstand Objekt x-Achse");
        Menu geoAbstandHeightMenu = new Menu ("Abstand Objekt y-Achse");


        //Create Slider as Custom Menu Item
        CustomMenuItem geoHeightMenuItem = new CustomMenuItem(geoHeightSlider);
        CustomMenuItem geoWidthMenuItem = new CustomMenuItem(geoWidthSlider);
        CustomMenuItem geoAbstandWidthMenuItem = new CustomMenuItem(geoAbstandWidthSlider);
        CustomMenuItem geoAbstandHeightMenuItem = new CustomMenuItem(geoAbstandHeightSlider);

        //Add as Sub Menu
        geoHeightMenu.getItems().add(geoHeightMenuItem);
        geoWidthMenu.getItems().add(geoWidthMenuItem);
        geoHeightMenuItem.setHideOnClick(false);
        geoWidthMenuItem.setHideOnClick(false);

        geoAbstandHeightMenu.getItems().add(geoAbstandHeightMenuItem);
        geoAbstandWidthMenu.getItems().add(geoAbstandWidthMenuItem);
        geoAbstandHeightMenuItem.setHideOnClick(false);
        geoAbstandWidthMenuItem.setHideOnClick(false);

        //Add to Configuration Menu
        configuration.getItems().add(geoHeightMenu);
        configuration.getItems().add(geoWidthMenu);
        configuration.getItems().add(geoAbstandHeightMenu);
        configuration.getItems().add(geoAbstandWidthMenu);

        configuration.getItems().add(rotaionMenu);

        Button saveImage = new Button("Bild speichern");
        Menu saveImageButton = new Menu("", saveImage);




        //Add color Picker and configuration
        menuBar.getMenus().addAll(drawButton, showText, showOverlay, colorPickers, configuration, saveImageButton);


        //Button mainSwitch = (Button) drawButton.getGraphic();

        //checkBox = (CheckBox) textShowCheckBox.getGraphic();

        colorPicker.setOnAction( event ->{ paneManager.setCanvasBackground(colorPicker.getValue()); });

        //Multiplikatoren eintragen
        Multiplikator multipli = new Multiplikator(geoWidthSlider.getValue(), geoHeightSlider.getValue(), geoAbstandWidthSlider.getValue(), geoAbstandHeightSlider.getValue());


        menuSave.setOnAction(event -> {
            saveEssay();
        });

        menuLoad.setOnAction(event -> {
            essay = loadEssay();
            paneManager.showEssayForm();
            paneManager.setEssayFormText(essay.getText());

        });

        mainSwitch.setOnMouseClicked(event -> {
            if(mainSwitch.getText().equals("Malen")) {
                mainSwitch.setText("Schreiben");
                paneManager.showCanvas(essay);
            }
            else if(mainSwitch.getText().equals("Schreiben")) {
                mainSwitch.setText("Malen");
                paneManager.showEssayForm();
            }
        });

        saveImage.setOnMouseClicked(event -> {
            saveSceneAsImage();
        });

        drawLettersCB.setOnAction(event -> {
            paneManager.showLetterByCheckbox(drawLettersCB.isSelected());
        });

        overlayCB.setOnAction(event -> {
            paneManager.showOverlayByCheckbox(overlayCB.isSelected());
        });

        rotationCB.setOnAction(event -> {
            paneManager.withRotation(rotationCB.isSelected());
        });

        // SLIDER ON CHANGE
        geoHeightSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //Change the multiplier here
                multipli.setMultiHeight((Double) newValue);
                paneManager.updateMultiplier(multipli);
            }
        });

        geoWidthSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //Change the multiplier here
                multipli.setMultiWidth((Double) newValue);
                paneManager.updateMultiplier(multipli);
            }
        });

        geoAbstandHeightSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //Change the multiplier here
                multipli.setyMulti((Double) newValue);
                paneManager.updateMultiplier(multipli);
            }
        });

        geoAbstandWidthSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //Change the multiplier here
                multipli.setxMulti((Double) newValue);
                paneManager.updateMultiplier(multipli);
            }
        });


        primaryStage.setScene(scene);
        primaryStage.show();
    }

    //Scene als Bild abspeichern
    public void saveSceneAsImage(){

        System.out.println("Bild wird abgespeichert");
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        Pane canvas = paneManager.getCanvas();
        //Show save file dialog
        File file = fileChooser.showSaveDialog(primaryStage);

        if(file != null){
            try {
                WritableImage writableImage = new WritableImage(((int) canvas.getWidth()), ((int)(canvas.getHeight())));
                canvas.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                Logger.getLogger(JavaFX_DrawOnCnvas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }


    public void saveEssay(){
        System.out.println("SAVE");

        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("JSON Datei (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(primaryStage);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try (PrintWriter pw = new PrintWriter(file.getPath())) {
            pw.println(gson.toJson(essay));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Essay loadEssay(){
        System.out.println("LOAD");
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("JSON Datei (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(primaryStage);

        String content = "";

        try {
            content = new String(Files.readAllBytes(Paths.get(file.getPath())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Essay loadedEssay = null;
        Essay loadedEssay = gson.fromJson(content, Essay.class);

        return loadedEssay;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
