package Controll;

public class Position {
    double xPos;
    double yPos;

    public Position() {
        xPos = 0;
        yPos = 0;
    }

    public Position(double x, double y){
        setXPos(x);
        setYPos(y);
    }

    public void setPosition(double x, double y) {
        setXPos(x);
        setYPos(y);
    }

    public void setXPos(double x){
        this.xPos = x;
    }

    public void setYPos(double y) {
        this.yPos = y;
    }

    public double getXPos(){
        return xPos;
    }

    public double getYPos(){
        return yPos;
    }
}
