package Controll;

import java.util.LinkedList;

public class Word {

    LinkedList<Letter> letterList = new LinkedList<Letter>();
    String word = "";

    public Word() {
    }

    public void addLetter(Letter letter) {
        if (letter.getText() != null) {
                word = word + letter.getText();
                letterList.add(letter);
        }
    }

    public String getWord(){
        return word;
    }

    public LinkedList<Letter> getLetterList() {
        return letterList;
    }
}