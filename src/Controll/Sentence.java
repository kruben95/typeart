package Controll;

import java.util.LinkedList;

public class Sentence {

    LinkedList<Word> wordList = new LinkedList<Word>();
    String sentence = "";
    String sentenceEnding = "";

    public Sentence(){
        Word currentWord = new Word();
        wordList.add(currentWord);
    }

    public void addLetter(Letter letter) {
        if (letter.getText() != null) {
            if (letter.getText().equals(" ")) {
                System.out.println("Neues Wort");
                Word currentWord = new Word();
                wordList.add(currentWord);
            }
            else {
                System.out.println("Kein Wortende");
                wordList.getLast().addLetter(letter);
            }
        }
    }

    public LinkedList<Word> getWordList() {
        return wordList;
    }

    public void setSentenceEnding(String ending) {
        System.out.println("Satzende: " + ending);
        this.sentenceEnding = ending;
    }
}
