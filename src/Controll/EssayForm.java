package Controll;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;

import java.io.IOException;
import java.util.EventListener;
import java.util.HashMap;

public class EssayForm {

    private TextArea essayForm;
    private TextArea testText;
    private Essay essay;
    private Scene scene;
    private PaneManager paneManager;
    private Letter lastLetter;

    private Event lastUndefinedEvent;

    public EssayForm(Scene scene, PaneManager paneManager, Essay essay) throws IOException {
        essayForm = FXMLLoader.load(getClass().getResource("/View/EssayForm.fxml"));
        this.essay = essay;
        this.scene = scene;
        this.paneManager = paneManager;

        setKeyListner();

        //Focus Textarea
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                essayForm.requestFocus();
            }
        });
    }

    public TextArea getPane(){
        return essayForm;
    }

    public void setKeyListner()
    {
        //Key: Character, Value: Event
        HashMap<String, Letter> eventHashMap = new HashMap<>();

        //ToDo: Komma "," muss noch hinzugeügt werden
        //ToDo: Groß/Kleinschreibung muss noch korrekt abgespeichert werden
        //ToDo: Umlaute müssen noch unterstützt werden

        essayForm.setOnKeyPressed(event -> {
            //System.out.println("name: " + event.getCode().name() + " getName: " + event.getCode().getName() + " toString: " + event.getCode().toString());

            //ignore if Modifier
            if (!igonredInput(event)) {

                Letter letter = new Letter(event);
                if(lastLetter!= null)letter.setLastKeyInputTime(lastLetter.getKeyPressStartTime());
                else letter.setLastKeyInputTime(letter.getKeyPressStartTime());

                //Special Key
                if (event.getText().equals("") || event.getText().equals(",")){
                    eventHashMap.put(event.getCode().name(), letter);
                }
                //Key with Modifier
                else if(event.isShiftDown()) eventHashMap.put(convertKeyWithModifier(event), letter);
                //Normal Letter
                else eventHashMap.put(event.getText(), letter);

                lastLetter = letter;

            }
        });

        essayForm.setOnKeyTyped(event -> {

            //ToDo: Spacebar doesn't trigger KeyPressed in the first place resulting in not finding the coresponding Event in the hashtable
            if(!event.getCharacter().equals(" ") && !event.getCharacter().equals("\t")) {
                eventHashMap.get(convertSpecialKey(event)).setKeyInputTime(System.nanoTime());
                eventHashMap.get(convertSpecialKey(event)).setCharacter(convertSpecialKey(event));
                eventHashMap.get(convertSpecialKey(event)).setLetter(convertSpecialKey(event));
                eventHashMap.get(convertSpecialKey(event)).setText(convertSpecialKey(event));

            }
        });

        essayForm.setOnKeyReleased(event -> {

            //ignore if Modifier
            System.out.println(event.getCode().name());
            if (!igonredInput(event)) {

                //ToDO: Komma noch schlecht implmentiert
                //Special Key
                if (event.getText().equals("") || event.getCode().name().equals("COMMA")) {
                    System.out.println(event.getCode().name());
                    eventHashMap.get(event.getCode().name()).setKeyPressStopTime(System.nanoTime());
                    essay.addLetter(eventHashMap.get(event.getCode().name()));
                }
                //Key with Modifier
                else if(event.isShiftDown()) {
                    eventHashMap.get(convertKeyWithModifier(event)).setKeyPressStopTime(System.nanoTime());
                    essay.addLetter(eventHashMap.get(convertKeyWithModifier(event)));
                }
                //Normal Letter
                else {
                    eventHashMap.get(event.getText()).setKeyPressStopTime(System.nanoTime());
                    essay.addLetter(eventHashMap.get(event.getText()));
                }

            }

            essay.setText(((TextArea)essayForm.lookup("#ta_TextEingabe")).getText());
            paneManager.updatePainCanvasEssay(essay);
        });
    }

    private boolean igonredInput(KeyEvent event) {
        if(event.getCode().name().equals("SHIFT")) return true;
        if(event.getCode().name().equals("ALT")) return true;
        if(event.getCode().name().equals("CONTROL")) return true;
        if(event.getCode().name().equals("ALT_GRAPH")) return true;
        if(event.getCode().name().equals("CAPS")) return true;
        if(event.getCode().name().equals("TAB")) return true;
        return false;
    }

    private String convertSpecialKey(KeyEvent event) {
        String key = event.getCharacter();
        switch (key) {
            case "\b":
                return "BACK_SPACE";
            case "\r":
                return "ENTER";
            case ",":
                return "COMMA";
            default:
                return key;
        }
    }

    private String convertKeyWithModifier(KeyEvent event){
        String key = event.getText();
        if(event.isControlDown() && event.isAltDown()) {
            switch (key) {
                case "1":
                    return "";
                case "2":
                    return "²";
                case "3":
                    return "³";
                case "4":
                    return "";
                case "5":
                    return "";
                case "6":
                    return "";
                case "7":
                    return "{";
                case "8":
                    return "[";
                case "9":
                    return "]";
                case "0":
                    return "}";
                case "ß":
                    return "\\";
                case "q":
                    return "@";
                default:
                    return key;
            }
        }
        if(event.isShiftDown()) {
            switch (key) {
                case "1":
                    return "!";
                case "2":
                    return "\"";
                case "3":
                    return "§";
                case "4":
                    return "$";
                case "5":
                    return "%";
                case "6":
                    return "&";
                case "7":
                    return "/";
                case "8":
                    return "(";
                case "9":
                    return ")";
                case "0":
                    return "=";
                case "ß":
                    return "?";
                case "a":
                    return "A";
                case "b":
                    return "B";
                case "c":
                    return "C";
                case "d":
                    return "D";
                case "e":
                    return "E";
                case "f":
                    return "F";
                case "g":
                    return "G";
                case "h":
                    return "H";
                case "i":
                    return "I";
                case "j":
                    return "J";
                case "k":
                    return "K";
                case "l":
                    return "L";
                case "m":
                    return "M";
                case "n":
                    return "N";
                case "o":
                    return "O";
                case "p":
                    return "P";
                case "q":
                    return "Q";
                case "r":
                    return "R";
                case "s":
                    return "S";
                case "t":
                    return "T";
                case "u":
                    return "U";
                case "v":
                    return "V";
                case "w":
                    return "W";
                case "x":
                    return "X";
                case "y":
                    return "Y";
                case "z":
                    return "Z";
                default:
                    return key;
            }
        }
        else return "";
    }

    public void setEssayFormText(String text){
        essayForm.setText(text);
    }
}
