package Controll;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import Geo.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

public class PaintCanvas {

    private Pane canvas;
    private Essay essay;
    private PaneManager paneManager;
    private boolean drawLettercheckBox;
    public static boolean withRotation = true;
    private TextArea textAreaIN, textAreaOUT;

    private Integer geoCounter = 0;
    private Multiplikator multiplier = new Multiplikator(1.0, 1.0, 1.0, 1.0);

    public void setMultiplier(Multiplikator multitplier) {
        this.multiplier = multitplier;
        paint();
    }

    public Multiplikator getMultiplier() {
        return multiplier;
    }

    HashMap<String, Geo> letterGeoHashMap = new HashMap<>();
    public Color backgroundColor = Color.WHITE;

    private GraphicsContext graphicsContext;

    public PaintCanvas(PaneManager paneManager) throws IOException {
        setUpLetterGeoHashMap();
        canvas = FXMLLoader.load(getClass().getResource("/View/Canvas.fxml"));
        canvas.setStyle("");
        this.paneManager = paneManager;
        paint();
    }

    public PaintCanvas(PaneManager paneManager, Essay essay) throws IOException {
        setUpLetterGeoHashMap();
        canvas = FXMLLoader.load(getClass().getResource("/View/Canvas.fxml"));
        this.paneManager = paneManager;
        this.essay = essay;
        paint();
    }

    public Pane getPane() {
        return canvas;
    }


    public void paint() {

        System.out.println("PAINT");

        graphicsContext = ((Canvas)canvas.getChildren().get(canvas.getChildren().size() - 2)).getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        drawShapes(graphicsContext);

        canvas.widthProperty().addListener(((observable, oldValue, newValue) -> {
            drawShapes(graphicsContext);
        }));

        canvas.heightProperty().addListener(((observable, oldValue, newValue) -> {
            drawShapes(graphicsContext);
        }));
    }

    private void drawShapes(GraphicsContext gc) {

        int sentenceCounter = 0;
        int wordCounter = 0;
        int letterCounter = 0;
        int yOffset = 5;

        if(essay == null) return;

        //UNNÖTIG ! HAT ZU FEHLERN GEFÜHRT!
        /*gc.clearRect(xPercent(0), yPercent(0), xPercent(100), yPercent(100));
        gc.setFill(backgroundColor);
        gc.fillRect(xPercent(0), yPercent(0), xPercent(100), yPercent(100));*/

        Position position = new Position(1,2);
        int pageCounter = 1;

        for (Sentence sentence : essay.getSentenceList()) {
            Position startSentencePos = new Position();
            Position endSentencePos = new Position();

            startSentencePos = position;
           // startSentencePos.setYPos();
            for(Word word : sentence.getWordList()) {
                Position startWordPos = new Position();
                Position endWordPos = new Position();

                startWordPos = position;

                for (Letter letter : word.getLetterList()) {

                    Geo geo = letterGeoHashMap.get(letter.letter);
                    geo.initialize(letter, gc, canvas, word.getWord(), wordCounter, letterCounter, multiplier);

                    //Wenn Ende einer Zeile, setze X auf 1 zurück und erhöhe Y
                    if(xPercent(position.getXPos()) > (canvas.getWidth()-xPercent(4))){
                        position.setYPos(position.getYPos() + yOffset*multiplier.getyMulti());
                        position.setXPos(1);
                    }
                    //Wenn Ende einer Seite erreicht, fange mit etwas mehr Offset wieder oben an
                    if(yPercent(position.getYPos()) > (canvas.getHeight()-yPercent(4))){
                        pageCounter++;
                        position.setYPos(pageCounter*4);
                    }

                    geo.setPosition(position);
                    position = geo.draw();
                    if (this.drawLettercheckBox) {
                        geo.setTextOnGeo();
                    }
                    endWordPos = position;

                    geoCounter++;
                    //gc.setFill(Color.RED);

                    //Zeichnet Quadrate irgendwo auf der auf dem Canvas (Soll später für einen Satz gelten?
                    /*gc.fillRect(    xPercent(startWordPos.getXPos()),
                                    yPercent(startWordPos.getYPos()),
                                xPercent(endWordPos.getXPos()-startWordPos.getXPos()),
                                yPercent(endWordPos.getYPos()-startWordPos.getYPos()));*/




                    letterCounter++;
                }

                //Erhöhe X, um einen Abstand zwischen 2 Wörtern zu erzeugen
                position.setXPos(position.getXPos()+5*multiplier.getxMulti());
                endSentencePos = position;

                wordCounter++;
                letterCounter = 0;
            }

            sentenceCounter++;
            wordCounter = 0;
            letterCounter = 0;
        }
    }

    private double xPercent(double x) {
        return x / 100 * canvas.getWidth();
    }

    private double yPercent(double y) {
        return y / 100 * canvas.getHeight();
    }

    public void updateEssay(Essay essay){
        this.essay = essay;
        paint();
    }

    public void showLetterByCheckbox(boolean checkBox) {

        this.drawLettercheckBox = checkBox;
        paint();
    }

    public void showOverlayByCheckbox(boolean checkBox) {
        if (checkBox) {
            textAreaOUT = (TextArea)getPane().lookup("#ta_TextAusgeben");
            textAreaOUT.setVisible(true);
            //textAreaOUT.setText(textAreaIN.getText());
            textAreaOUT.setText(essay.getText());
        }
        else{
            textAreaOUT.setVisible(false);
        }

        paint();
    }

    public void withRotation(boolean checkbox) {
        this.withRotation = checkbox;
        paint();
    }

    public void setBackgroundColor(Color color){
        canvas.setStyle("-fx-background-color: #"+ color.toString().substring(2,color.toString().length()));
    }

    private void setUpLetterGeoHashMap(){
        letterGeoHashMap.put("a", new SymbolA());
        letterGeoHashMap.put("b", new SymbolB());
        letterGeoHashMap.put("c", new SymbolC());
        letterGeoHashMap.put("d", new SymbolD());
        letterGeoHashMap.put("e", new SymbolE());
        letterGeoHashMap.put("f", new SymbolF());
        letterGeoHashMap.put("g", new SymbolG());
        letterGeoHashMap.put("h", new SymbolH());
        letterGeoHashMap.put("i", new SymbolI());
        letterGeoHashMap.put("j", new SymbolJ());
        letterGeoHashMap.put("k", new SymbolK());
        letterGeoHashMap.put("l", new SymbolL());
        letterGeoHashMap.put("m", new SymbolM());
        letterGeoHashMap.put("n", new SymbolN());
        letterGeoHashMap.put("o", new SymbolO());
        letterGeoHashMap.put("p", new SymbolP());
        letterGeoHashMap.put("q", new SymbolQ());
        letterGeoHashMap.put("r", new SymbolR());
        letterGeoHashMap.put("s", new SymbolS());
        letterGeoHashMap.put("t", new SymbolT());
        letterGeoHashMap.put("u", new SymbolU());
        letterGeoHashMap.put("v", new SymbolV());
        letterGeoHashMap.put("w", new SymbolW());
        letterGeoHashMap.put("x", new SymbolX());
        letterGeoHashMap.put("y", new SymbolY());
        letterGeoHashMap.put("z", new SymbolZ());


        letterGeoHashMap.put("A", new SymbolA());
        letterGeoHashMap.put("B", new SymbolB());
        letterGeoHashMap.put("C", new SymbolC());
        letterGeoHashMap.put("D", new SymbolD());
        letterGeoHashMap.put("E", new SymbolE());
        letterGeoHashMap.put("F", new SymbolF());
        letterGeoHashMap.put("G", new SymbolG());
        letterGeoHashMap.put("H", new SymbolH());
        letterGeoHashMap.put("I", new SymbolI());
        letterGeoHashMap.put("J", new SymbolJ());
        letterGeoHashMap.put("K", new SymbolK());
        letterGeoHashMap.put("L", new SymbolL());
        letterGeoHashMap.put("M", new SymbolM());
        letterGeoHashMap.put("N", new SymbolN());
        letterGeoHashMap.put("O", new SymbolO());
        letterGeoHashMap.put("P", new SymbolP());
        letterGeoHashMap.put("Q", new SymbolQ());
        letterGeoHashMap.put("R", new SymbolR());
        letterGeoHashMap.put("S", new SymbolS());
        letterGeoHashMap.put("T", new SymbolT());
        letterGeoHashMap.put("U", new SymbolU());
        letterGeoHashMap.put("V", new SymbolV());
        letterGeoHashMap.put("W", new SymbolW());
        letterGeoHashMap.put("X", new SymbolX());
        letterGeoHashMap.put("Y", new SymbolY());
        letterGeoHashMap.put("Z", new SymbolZ());


        letterGeoHashMap.put("BACK_SPACE", new SymbolBackspace());
        letterGeoHashMap.put("TAB", new Symbol("OTHER"));
        letterGeoHashMap.put("CAPS", new Symbol("OTHER"));
        letterGeoHashMap.put("COMMA", new Symbol("OTHER"));
        letterGeoHashMap.put("ENTER", new Symbol("OTHER"));
    }
}
