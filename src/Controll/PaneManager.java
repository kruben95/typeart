package Controll;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.IOException;


public class PaneManager {

    private Main main;
    private Scene scene;
    private Essay essay;
    private PaintCanvas paintCanvas;
    private EssayForm essayForm;
    private Pane container;
    private VBox mainVBox;

    public PaneManager(Main main, Scene scene, Essay essay) throws IOException {
        this.main = main;
        this.scene = scene;
        this.essay = essay;

        paintCanvas = new PaintCanvas(this);
        essayForm = new EssayForm(scene, this, essay);
        mainVBox = (VBox) scene.lookup("#mainVBox");
        container = (Pane) scene.lookup("#paneContainer");

        mainVBox.prefHeightProperty().bind(scene.heightProperty());
        mainVBox.prefWidthProperty().bind(scene.widthProperty());

        container.prefHeightProperty().bind(mainVBox.heightProperty());
        container.prefWidthProperty().bind(mainVBox.widthProperty());

        showEssayForm();
    }

    public void showEssayForm(){

        if(container.getChildren().size() > 0) container.getChildren().remove(0);
        container.getChildren().add(0, essayForm.getPane());

        essayForm.getPane().prefHeightProperty().bind(((Pane)essayForm.getPane().getParent()).heightProperty());
        essayForm.getPane().prefWidthProperty().bind(((Pane)essayForm.getPane().getParent()).widthProperty());
    }

    public void showCanvas(Essay essay){

        this.essay = essay;

        if(container.getChildren().size() > 0) container.getChildren().remove(0);
        container.getChildren().add(0, paintCanvas.getPane());

        paintCanvas.getPane().prefHeightProperty().bind(((Pane)paintCanvas.getPane().getParent()).heightProperty());
        paintCanvas.getPane().prefWidthProperty().bind(((Pane)paintCanvas.getPane().getParent()).widthProperty());

        //bind textarea to pane
        ((TextArea) paintCanvas.getPane().getChildren().get(1)).prefWidthProperty().bind(paintCanvas.getPane().widthProperty());
        ((TextArea) paintCanvas.getPane().getChildren().get(1)).prefHeightProperty().bind(paintCanvas.getPane().heightProperty());

        //bind canvas to pane
        ((Canvas) paintCanvas.getPane().getChildren().get(0)).widthProperty().bind(paintCanvas.getPane().widthProperty());
        ((Canvas) paintCanvas.getPane().getChildren().get(0)).heightProperty().bind(paintCanvas.getPane().heightProperty());

        paintCanvas.updateEssay(essay);
    }

    public void setEssayFormText(String text){
        essayForm.setEssayFormText(text);
    }

    public void updateMultiplier(Multiplikator multi) {
        paintCanvas.setMultiplier(multi);
    }

    public void setCanvasBackground(Color color) {
        paintCanvas.setBackgroundColor(color);
    }

    public void updatePainCanvasEssay(Essay essay)
    {
        paintCanvas.updateEssay(essay);
    }

    public void showLetterByCheckbox(boolean checkBox) {
        paintCanvas.showLetterByCheckbox(checkBox);
    }

    public void showOverlayByCheckbox(boolean checkBox) {
        paintCanvas.showOverlayByCheckbox(checkBox);
    }

    public void withRotation(boolean checkbox){
        paintCanvas.withRotation(checkbox);
    }

    public Pane getCanvas (){ return paintCanvas.getPane(); }
}
