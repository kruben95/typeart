package Controll;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Letter {

    String letter;

    private long lastKeyInputTime;
    private long keyInputTime;
    private long keyPressStartTime;
    private long keyPressStopTime;

    private long timeSinceLastInput;
    private long timeKeyHoldDown;

    private String text;
    private KeyCode code;
    private String character;
    private String codeCH;

    public Letter(Letter letter){
        this.letter = letter.letter;
        this.lastKeyInputTime = letter.lastKeyInputTime;
        this.keyInputTime = letter.keyInputTime;
        this.keyPressStartTime = letter.keyPressStartTime;
        this.keyPressStopTime = letter.keyPressStopTime;
        //this.timeSinceLastInput = letter.timeSinceLastInput;
        //this.timeKeyHoldDown = letter.timeKeyHoldDown;
    }

    public Letter(KeyEvent keyEvent){
        this.text = keyEvent.getText();
        this.code = keyEvent.getCode();
        this.keyPressStartTime = System.nanoTime();
    }

    public void initialize(){
        timeSinceLastInput = keyPressStartTime - lastKeyInputTime;
        timeKeyHoldDown = keyPressStopTime - keyPressStartTime;
    }

    public long getLastKeyInputTime() {
        return lastKeyInputTime;
    }

    public void setLastKeyInputTime(long lastKeyInputTime) {
        this.lastKeyInputTime = lastKeyInputTime;
    }

    public long getKeyInputTime() {
        return keyInputTime;
    }

    public void setKeyInputTime(long keyInputTime) {
        this.keyInputTime = keyInputTime;
    }

    public long getKeyPressStartTime() {
        return keyPressStartTime;
    }

    public void setKeyPressStartTime(long keyPressStartTime) {
        this.keyPressStartTime = keyPressStartTime;
    }

    public long getKeyPressStopTime() {
        return keyPressStopTime;
    }

    public void setKeyPressStopTime(long keyPressStopTime) {
        this.keyPressStopTime = keyPressStopTime;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public KeyCode getCode() {
        return code;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getCodeCH() {
        return codeCH;
    }

    public void setCodeCH(String codeCH) {
        this.codeCH = codeCH;
    }
}
