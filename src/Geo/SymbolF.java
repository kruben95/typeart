package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolF extends Geo implements Drawable {

    protected double width = 7 ;
    protected double height  = 4;

    @Override
    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getHeight() {
        return this.height;
    }

    @Override
    public Position draw() {

        setColor(Color.rgb(230, 230, 250));
        if(isUppercase()) height = 6 * multipli.getMultiHeight();
        else height = 4 * multipli.getMultiHeight();
        width = 7 * multipli.getMultiWidth();

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        double[] xValues = new double[] {xPercent((width/3*1) + position.getXPos()), xPercent((width/3*2) + position.getXPos()), xPercent((width/3*3) + position.getXPos()), xPercent((width/3*3) + position.getXPos()), xPercent((width/3*2) + position.getXPos()), xPercent((width/3*1) + position.getXPos()), xPercent((width/3*0) + position.getXPos()), xPercent((width/3*0) + position.getXPos())};
        double[] yValues = new double[] {yPercent((height/3*0) + position.getYPos()), yPercent((height/3*0) + position.getYPos()), yPercent((height/3*1) + position.getYPos()), yPercent((height/3*2) + position.getYPos()), yPercent((height/3*3) + position.getYPos()), yPercent((height/3*3) + position.getYPos()), yPercent((height/3*2) + position.getYPos()), yPercent((height/3*1) + position.getYPos())};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}