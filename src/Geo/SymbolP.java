package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolP extends Geo implements Drawable {

    protected double width = 3 ;
    protected double height  = 6;

    @Override
    public Position draw() {

        setColor(Color.rgb(255, 192, 203));
        if(isUppercase()) height = 8 * multipli.getMultiHeight();
        else height = 6 * multipli.getMultiHeight();
        width = 3 * multipli.getMultiWidth();

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        double[] xValues = new double[] {xPercent((width/3*1) + position.getXPos()), xPercent((width/3*2) + position.getXPos()), xPercent((width/3*3) + position.getXPos()), xPercent((width/3*3) + position.getXPos()), xPercent((width/3*2) + position.getXPos()), xPercent((width/3*1) + position.getXPos()), xPercent((width/3*0) + position.getXPos()), xPercent((width/3*0) + position.getXPos())};
        double[] yValues = new double[] {yPercent((height/3*0) + position.getYPos()), yPercent((height/3*0) + position.getYPos()), yPercent((height/3*1) + position.getYPos()), yPercent((height/3*2) + position.getYPos()), yPercent((height/3*3) + position.getYPos()), yPercent((height/3*3) + position.getYPos()), yPercent((height/3*2) + position.getYPos()), yPercent((height/3*1) + position.getYPos())};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);
        gc.setStroke(color.brighter());
        gc.strokePolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}