package Geo;

import Controll.Position;

public class Circle extends Geo implements Drawable {

    public Circle(){}

    @Override
    public Position draw() {

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);

        gc.save();
        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));

        gc.restore();

        //add width of circle
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}
