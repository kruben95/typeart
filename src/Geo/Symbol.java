package Geo;
import Controll.Position;
import javafx.scene.paint.Color;


public class Symbol extends Geo implements Drawable {

    private String zeichen;


    public Symbol(String zeichen){
        this.zeichen = zeichen;
    }

    @Override
    public Position draw() {

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);

        //Abfrage welches Zeichen auf der Tastatur gezeichnet wird und zeichnet dann das Geo Objekt
         switch(zeichen){
             case "A":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(127, 255, 212));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 break;
             case "B":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(0, 0, 255));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 //gc.fillPolygon(new double[]{xPercent(x), yPercent(y)},
                         //new double[]{xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight())}, 3);
                 gc.restore();

                 break;
             case "C":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(0, 255, 255));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();


                 break;
             case "D":
                 //Farbe setzen auf den RGB Wert des Buchstaben

                 //Form Zeichenen
                 setColor(Color.rgb(0, 0, 139));
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "E":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 255, 240));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "F":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(230, 230, 250));
                 //Form Zeichenen

                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "G":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(0, 128, 0));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen A: "+ x + "und Y: " + y);
                 break;
             case "H":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(135, 206, 235));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "I":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(75, 0, 130));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "J":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(102, 205, 170));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "K":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(128, 0, 0));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "L":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(219, 112, 147));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "M":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 0, 255));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "N":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(0, 0, 128));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "O":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 165, 0));
                 //addColorMultiplier(color.getRed());
                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.strokeOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "P":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 192, 203));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "Q":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(105, 105, 105));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "R":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 0, 0));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "S":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(192, 192, 192));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "T":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(64, 224, 208));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "U":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(139, 69, 19));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "V":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(238, 130, 238));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "W":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(245, 222, 179));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case "X":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(89, 107, 47));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case"Y":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(244, 164, 96));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
             break;
             case"Z":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 250, 205));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             case"OTHER":
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(0, 0, 0));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen "+ zeichen +": "+ x + "und Y: " + y);
                 break;
             default:
                 //Farbe setzen auf den RGB Wert des Buchstaben
                 setColor(Color.rgb(255, 0, 0));

                 //Form Zeichenen
                 gc.save();
                 gc.translate(rotationCenterX, rotationCenterY);
                 gc.rotate(rotation);
                 gc.translate(-rotationCenterX, -rotationCenterY);
                 gc.fillOval(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));
                 gc.restore();

                 System.out.println("Zeichen INVALID!: "+ x + "und Y: " + y);
                 break;
             //add width of circle

         }


        //Multiplikator auf die Farbe anwenden

        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}