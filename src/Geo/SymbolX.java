package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolX extends Geo implements Drawable {

    protected double width = 4 ;
    protected double height  = 4;

    @Override
    public Position draw() {

        setColor(Color.rgb(89, 107, 47));
        if(isUppercase()){
            height = 6 * multipli.getMultiHeight();
            width = 6 * multipli.getMultiWidth();
        }
        else {
            height = 4 * multipli.getMultiHeight();
            width = 4 * multipli.getMultiWidth();
        }

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        double[] xValues = new double[] {xPercent((width/3*0) + position.getXPos()), xPercent((width/3*3) + position.getXPos()), xPercent((0) + position.getXPos()), xPercent((width) + position.getXPos())};
        double[] yValues = new double[] {yPercent((0) + position.getYPos()), yPercent((0) + position.getYPos()), yPercent((height) + position.getYPos()), yPercent((height) + position.getYPos())};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);
        gc.setStroke(color.brighter());
        gc.strokePolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}