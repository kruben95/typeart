package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolE extends Geo implements Drawable {

    protected double width = 5 ;
    protected double height  = 5;

    @Override
    public Position draw() {

        setColor(Color.rgb(255, 255, 240));
        if(isUppercase()) height = 7 * multipli.getMultiHeight();
        else height = 5 * multipli.getMultiHeight();
        width = 5 * multipli.getMultiWidth();

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        gc.setStroke(color.darker().darker());
        gc.strokeRoundRect(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()), 10, 10);
        gc.fillRoundRect(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()), 10, 10);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}