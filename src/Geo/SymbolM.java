package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolM extends Geo implements Drawable {

    protected double width = 8 ;
    protected double height  = 4;

    @Override
    public Position draw() {

        setColor(Color.rgb(255, 0, 255));
        if(isUppercase()) height = 6 * multipli.getMultiHeight();
        else height = 4 * multipli.getMultiHeight();
        width = 8 * multipli.getMultiWidth();

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        double[] xValues = new double[] {xPercent((width/4) + position.getXPos()), xPercent((width) + position.getXPos()), xPercent((width-(width/4)) + position.getXPos()), xPercent((0) + position.getXPos())};
        double[] yValues = new double[] {yPercent((0) + position.getYPos()), yPercent((0) + position.getYPos()), yPercent((height) + position.getYPos()), yPercent((height) + position.getYPos())};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}