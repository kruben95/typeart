package Geo;

import Controll.Letter;
import Controll.Multiplikator;
import Controll.PaintCanvas;
import Controll.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import sun.plugin.dom.css.RGBColor;

public class Geo {

    protected Letter letter;
    private Pane canvas;
    public GraphicsContext gc;

    protected double x;
    protected double y;
    protected double width = 5 ;
    protected double height  = 5;
    protected double rotation = 0;
    protected double transparency = 1;
    protected String word = "";
    protected int wordCounter;
    protected int letterCounter;

    protected Color color = Color.rgb(255, 255, 0, 1);


    protected Position position;
    protected Multiplikator multipli = new Multiplikator(1.0,1.0,1.0,1.0);

    public Double getMultiHeight() {
        return multipli.getMultiHeight();
    }

    public Double getMultiWidth() {
        return multipli.getMultiWidth();
    }


    public Double getxMulti() {
        return multipli.getxMulti();
    }

    public Double getyMulti() {
        return multipli.getyMulti();
    }

    public Color getColor() {
        return color;
    }

    public Geo() {
    }

    public Geo(Pane canvas, GraphicsContext gc){
        this.canvas = canvas;
        this.gc = gc;
    }

    public void initialize(Letter letter, GraphicsContext gc, Pane canvas, String word, int wordCounter, int letterCounter, Multiplikator multipli) {
        this.letter = letter;
        this.canvas = canvas;
        this.word = word;
        this.wordCounter = wordCounter;
        this.letterCounter = letterCounter;
        this.gc = gc;
        if(multipli != null) this.multipli = multipli;

        calculateXMulti();

        //ToDo: Transparenz noch mit richtiger Fromel berechnen
        setTransparency((double)(wordCounter+1)/(letterCounter+3));

        setRotation((double)360/(letter.getKeyPressStopTime()-letter.getKeyPressStartTime())*20000000);
    }

    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public double getWidth() { return width * multipli.getMultiWidth(); }

    public void setWidth(double width) { this.width = width; }

    public double getHeight() { return height * multipli.getMultiHeight();    }

    public void setHeight(double height) { this.height = height;    }

    public void setPosition(double x, double y){
        setX(x);
        setY(y);
        position.setXPos(x);
        position.setYPos(y);
    }

    public void setPosition(Position position) {
        this.position = position;
        setX(position.getXPos());
        setY(position.getYPos());

    }

    public double xPercent(double x) {
        return x / 100 * canvas.getWidth();
    }

    public double yPercent(double y) {
        return y / 100 * canvas.getHeight();
    }

    public void setTransparency(double transparency) {
        this.transparency = transparency;
        gc.setGlobalAlpha(transparency);
    }

    public void setColor(Color color) {
        this.color = color;
        calculateRedMultiplier();
        calculateGreenMultiplier();
        calculateBlueMultiplier();
        addColorMultiplier();
        gc.setFill(getColor());
        gc.setStroke(getColor());
    }

    //Old Min-Max Range, New Min-Max Range, oldValue, invert value 0,8 -> 0.2
    //return newValue
    public double doubleMap(double currentValue, double currentMin, double currentMax, double newMin, double newMax, boolean invert)
    {
        if(currentValue > currentMax) currentValue = currentMax;
        if(currentValue < currentMin) currentValue = currentMin;
        if(invert) currentValue = (currentMax+currentMin) - currentValue;

        return newMin + (newMax - newMin) * ((currentValue - currentMin) / (currentMax - currentMin));
    }

    public void calculateRedMultiplier() {
        //Min-Max Range for Keyinputs
        int minOld = 30000000;
        int maxOld = 250000000;

        //New Min-Max Range for Multiplicator
        double minNew = 0.6;
        double maxNew = 1.4;

        //Time between keypress and keyrelease
        int currentValue = (int)(letter.getKeyPressStopTime() - letter.getKeyPressStartTime());

        multipli.setMultiRed(doubleMap(currentValue, minOld, maxOld, minNew, maxNew, true));
    }

    public void calculateGreenMultiplier() {
        int minOld = 3;
        int maxOld = 18;

        double minNew = 0.6;
        double maxNew = 1.4;

        int currentValue = (word.length());

        multipli.setMultiGreen(doubleMap(currentValue, minOld, maxOld, minNew, maxNew, false));
    }

    public void calculateBlueMultiplier() {
        int minOld = 1;
        int maxOld = 27;

        double minNew = 0.6;
        double maxNew = 1.4;

        int currentValue = getLetterRarity(((String) word.subSequence(0,1)));

        multipli.setMultiBlue(doubleMap(currentValue, minOld, maxOld, minNew, maxNew, false));
    }

    public void calculateXMulti() {

        double minOld = 30000000;
        double maxOld = 5000000000.0;

        double minNew = 0.8;
        double maxNew = 3;

        double currentValue = (letter.getKeyPressStartTime() - letter.getLastKeyInputTime());

        multipli.setxMulti2(doubleMap(currentValue, minOld, maxOld, minNew, maxNew, false));
    }

    public void addColorMultiplier (){
        double red = doubleMap(color.getRed()*multipli.getMultiRed(), 0, 1, 0, 255, false);
        double green = doubleMap(color.getGreen()*multipli.getMultiGreen(), 0, 1,0,255, false);
        double blue = doubleMap(color.getBlue()*multipli.getMultiBlue(), 0, 1,0,255, false);

        color =  Color.rgb((int)red,(int)green,(int)blue);
    }

    public Position draw() {
        return null;
    }

    public void setRotation(double roatation){
        if(PaintCanvas.withRotation) this.rotation = roatation;
        else this.rotation = 0;
    }

    public void setTextOnGeo(){

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Verdana", 20));
        gc.fillText(letter.getLetter(), xPercent(position.getXPos()) + xPercent((width*multipli.getMultiWidth())/2-1), yPercent(position.getYPos()) + yPercent((height *multipli.getMultiHeight())/2));

    }

    public boolean isUppercase() {
        return (letter.getText().equals(letter.getText().toUpperCase()));
    }

    public int getLetterRarity(String letter){
        switch (letter.toUpperCase()){
            case "A":
                return 7;
            case "B":
                return 16;
            case "C":
                return 11;
            case "D":
                return 9;
            case "E":
                return 1;
            case "F":
                return 17;
            case "G":
                return 13;
            case "H":
                return 8;
            case "I":
                return 3;
            case "J":
                return 23;
            case "K":
                return 20;
            case "L":
                return 12;
            case "M":
                return 14;
            case "N":
                return 2;
            case "O":
                return 15;
            case "P":
                return 22;
            case "Q":
                return 25;
            case "R":
                return 4;
            case "S":
                return 6;
            case "T":
                return 5;
            case "U":
                return 10;
            case "V":
                return 21;
            case "W":
                return 18;
            case "X":
                return 24;
            case "Y":
                return 26;
            case "Z":
                return 19;
            default:
                return 27;
        }
    }
}