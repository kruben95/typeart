package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolJ extends Geo implements Drawable {

    protected double width = 5 ;
    protected double height  = 5;

    @Override
    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getHeight() {
        return this.height;
    }

    @Override
    public Position draw() {

        setColor(Color.rgb(102, 205, 170));
        if(isUppercase()) setHeight(7);
        else setHeight(5);

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        gc.fillRect(xPercent(x), yPercent(y), xPercent(getWidth() * getMultiWidth()), yPercent(getHeight() * getMultiHeight()));

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}