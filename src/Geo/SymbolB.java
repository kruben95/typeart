package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolB extends Geo implements Drawable {

    protected double width = 3 ;
    protected double height  = 5;

    @Override
    public Position draw() {

        setColor(Color.rgb(0, 0, 255));
        if(isUppercase()){
            width = 5 * multipli.getMultiWidth();
            height = 6 * multipli.getMultiHeight();
        }
        else {
            width = 3 * multipli.getMultiWidth();
            height = 5 * multipli.getMultiHeight();
        }

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        double[] xValues = new double[] {xPercent((width/4*1) + position.getXPos()), xPercent((width) + position.getXPos()), xPercent((width/4*1) + position.getXPos())};
        double[] yValues = new double[] {yPercent((0) + position.getYPos()), yPercent((height) + position.getYPos()), yPercent((height) + position.getYPos())};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}