package Geo;

import Controll.Position;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class SymbolI extends Geo implements Drawable {

    @Override
    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getHeight() {
        return this.height;
    }

    @Override
    public Position draw() {

        double width = 5 ;
        double height  = 5;

        setColor(Color.rgb(75, 0, 130));
        if(isUppercase()) height = 8;
        else height = 5;

        double heightMulti = 1.0;
        if(multipli.getMultiWidth() > multipli.getMultiHeight()) heightMulti = multipli.getMultiWidth();
        else heightMulti = multipli.getMultiHeight();

        double rotationCenterX = xPercent(position.getXPos() + (width * heightMulti) / 2);
        double rotationCenterY = yPercent(position.getYPos() + (height * heightMulti) / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);


        height = height * heightMulti;
        width = width * heightMulti;
        double xPos = position.getXPos();
        double yPos = position.getYPos();

        //Dreieck

        //Großes Dreieck
        double hypothenusC1 = Math.sqrt(Math.pow(height/2, 2) + Math.pow(height/2, 2));
        double katheteA1 = height/2;

        //kleines Dreieck
        double hypothenuseC2 = height/2;
        double katheteA2 = hypothenusC1/2;
        double katheteB2 = Math.sqrt(Math.pow(hypothenuseC2, 2) - Math.pow(katheteA2, 2));

        double[] xValues = new double[] {xPercent((width/2) + xPos), xPercent((width/2+katheteA2) + xPos), xPercent((width/2-katheteA2) + xPos)};
        double[] yValues = new double[] {yPercent(0.0 + yPos), yPercent((height/2+katheteB2) + yPos), yPercent((height/2+katheteB2) + yPos)};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}