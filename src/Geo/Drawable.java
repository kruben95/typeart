package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public interface Drawable {

    void setColor(Color color);
    void setX(double x);
    void setY(double y);
    void setPosition(double x, double y);
    void setWidth(double width);
    void setHeight(double height);
    void setRotation(double rotation);
    void setTransparency(double transparency);
    Position draw();
}
