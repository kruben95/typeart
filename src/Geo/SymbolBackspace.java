package Geo;

import Controll.Position;
import javafx.scene.paint.Color;

public class SymbolBackspace extends Geo implements Drawable {

    protected double width = 7 ;
    protected double height  = 6;

    @Override
    public Position draw() {

        setColor(Color.rgb(255, 0, 0));
        if(isUppercase()){
            width = 7 * multipli.getMultiWidth();
            height = 7 * multipli.getMultiHeight();
        }
        else {
            width = 7 * multipli.getMultiWidth();
            height = 6 * multipli.getMultiHeight();
        }

        position.setPosition(position.getXPos()-5*multipli.getxMulti()+1, position.getYPos()-1);

        double rotationCenterX = xPercent(position.getXPos() + getWidth() * getMultiWidth() / 2);
        double rotationCenterY = yPercent(position.getYPos() + getHeight() * getMultiHeight() / 2);
        gc.save();

        gc.translate(rotationCenterX, rotationCenterY);
        gc.rotate(rotation);
        gc.translate(-rotationCenterX, -rotationCenterY);

        double[] xValues = new double[] {xPercent((0) + position.getXPos()), xPercent((width/7*3) + position.getXPos()), xPercent((width/7*5) + position.getXPos()), xPercent((width) + position.getXPos()), xPercent((width) + position.getXPos()), xPercent((width/7*5) + position.getXPos()), xPercent((width/7*3) + position.getXPos()), xPercent((0) + position.getXPos())};
        double[] yValues = new double[] {yPercent((height/7*4) + position.getYPos()), yPercent((height/7*3) + position.getYPos()), yPercent((height/7*4) + position.getYPos()), yPercent((height/7*3) + position.getYPos()), yPercent((height/7*4) + position.getYPos()), yPercent((height/7*5) + position.getYPos()), yPercent((height/7*4) + position.getYPos()), yPercent((height/7*5) + position.getYPos())};
        int nPoint = xValues.length;

        gc.fillPolygon(xValues, yValues , nPoint);

        gc.restore();
        int i = 0;
        i++;
        return new Position(x + 5 * getxMulti() * multipli.getxMulti2(), y);
    }
}
