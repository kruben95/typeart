## Notiz für Grafik Objekte

1. Verschiedene Geo Objekte für Buchstaben usw überlegen und dem Programm hinzufügen. 
	1.1 Buchstaben A-Z, a-z -----> Großbuchstabe = Größere Form
	1.2 Satzende (!,.,?)
	1.3 Löschen (Backspace)
2. Formel für Farben überlegen[yes] und implementieren[Done: no]
3. Formel für Rotation überlegen und implementieren[Kann überarbeitet werden]
4. Kleiner vorbestimmter Wert für Breite und Höhe durch Formel definieren??[Formel: Yes] [Implementiert: No]
6. Satzende durch zusätliche Form darstellen oder durch Transparency zeigen[Form : Yes] [Implementiert: NO]
7. Abstaz darstellen und im Code implementieren? --> Nächste Zeile [Implementiert: No]



## Eingabemöglichkeiten und mögliche auszulesende Werte

1. Wie lange wurde eine Taste gedrückt?  Größere Form in y RIchtung (breite) --> 0ns - 100.000ns (100ms) | Breitemultipli.: 65.000/100.00 *0,5+1
2. Wie lange ist es her das der letzte Buchstabe gedrückt wurde?  -- > Multiplikator für die [Rote] Farbe |( (Grundwert des Buchstaben)/2 * Zeit [Werte 0.0-2.0])
3. Anzahl der Wörter insgemsamt. -> Farbe des Satzendes ( Hintergrunnd des Satzes)
4. Anzahl der Buchstben/Zeichen imsgesamt pro Wort--> Multiplikator für Farbe [Grün] | ((Wort.nzahal.Buchstaben / festgelegterMaxWert) * Multiplikator[Wert 0,5])
5. Wie viele Sätze insgesammt? Farbe des Satzendes ( Hintergrunnd des Satzes)
6. Buchstaben des Textes insgesammt?
7. Wie viele Großbuchstaben im Text?
8. Großbuchstabe -->> Größere Form x (Höhe)
9. Wörter pro Satz?
10. Erster Buchstbe des Wortes --> Multiplikator Farbe [Blau] | A-Z .. 1-26 ergibt [Werte 0 - 1] (26 / Buchstaben.Index + 0,5) = Blauwert

## DONE
1. Weiterschreiben..
	1.1 Save
	1.2 Load
2. Erschaffenes als PNG abspeichern[Done: Yes]
3. Farben auf Buchstaben festlegen

## Notiz Formel Position Berechnung
MAX Wert: 5 Sekunden
Min Offset: -2%
Max Offset: 5%


Bsp. InputTime: 0,01s
0,01

## Canvas abspeichern
http://java-buddy.blogspot.com/2013/04/save-canvas-to-png-file.html
